package com.mistiot.vehicleapi.mock_test;

/**
 * @author Koshan Samarasinghe
 * @date Oct 06, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.config.CommonConfig;
import com.mistiot.vehicleapi.service.VehicleService;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.ResourceUtils;
import java.io.File;
import java.nio.file.Files;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class VehicleControllerMockTest extends CommonConfig {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testA_SaveVehicle() throws Exception {
        File file = ResourceUtils.getFile("src/test/resources/vehicle.json");
        String content = new String(Files.readAllBytes(file.toPath()));
        mockMvc.perform(MockMvcRequestBuilders.post("/v1/vehicle")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testB_FindVehicleByType() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/v1/vehicle/{type}", "CAR")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isFound());
    }
}
