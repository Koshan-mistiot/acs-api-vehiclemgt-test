package com.mistiot.vehicleapi.repo_test;

/**
 * @author Koshan Samarasinghe
 * @date Oct 07, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.model.Vehicle;
import com.mistiot.vehicleapi.repository.VehicleRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class VehicleRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private VehicleRepo vehicleRepo;

    @Test
    public void whenFindByType_thenReturnVehicle() {

        Vehicle vehicleObj = new Vehicle(1, "VFLB4534745432", "CAR", "Brand new Nissan car from japan");
        entityManager.persist(vehicleObj);
        entityManager.flush();
        Vehicle found = vehicleRepo.findByType(vehicleObj.getType());
        assertThat(found.getType()).isEqualTo(vehicleObj.getType());
    }

//    @Test
//    public void testB_whenFindByType_thenReturnVehicle() {
//
//        Vehicle vehicleObj = new Vehicle(1, "VFLB4534745432", "CAR", "Brand new Nissan car from japan");
//        Vehicle savedObj = vehicleRepo.save(vehicleObj);
//        Vehicle found = vehicleRepo.findByType(vehicleObj.getType());
//        Assert.assertEquals(savedObj.getType(), found.getType());
//    }
}