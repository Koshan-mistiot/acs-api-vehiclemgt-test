package com.mistiot.vehicleapi.service_test;

/**
 * @author Koshan Samarasinghe
 * @date Oct 07, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.common.ResourcePack;
import com.mistiot.vehicleapi.dto.VehicleDto;
import com.mistiot.vehicleapi.model.Vehicle;
import com.mistiot.vehicleapi.repository.VehicleRepo;
import com.mistiot.vehicleapi.service.VehicleService;
import com.mistiot.vehicleapi.service.impl.VehicleServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
public class VehicleServiceTest {

    @TestConfiguration
    static class VehicleServiceImplTestContextConfiguration {
        @Bean
        public VehicleService vehicleService() {
            return new VehicleServiceImpl();
        }
    }

    @Autowired
    private VehicleService vehicleService;
    @MockBean
    private VehicleRepo vehicleRepo;

    @Before
    public void setUp() {
        Vehicle vehicleObj = new Vehicle(1, "VFLB4534745432", "CAR", "Brand new Nissan car from japan");
        Mockito.when(vehicleRepo.findByType(vehicleObj.getType()))
                .thenReturn(vehicleObj);
    }
    @Test
    public void whenValidType_thenVehicleShouldBeFound() {
        String type = "CAR";
        ResourcePack<VehicleDto> found = vehicleService.findByVehicleType(type);
        assertThat(found.getData().getType())
                .isEqualTo(type);
    }
}
