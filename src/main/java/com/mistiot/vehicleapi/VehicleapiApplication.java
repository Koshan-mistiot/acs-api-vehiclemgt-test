package com.mistiot.vehicleapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleapiApplication {

    private static Logger logger = LoggerFactory.getLogger(VehicleapiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(VehicleapiApplication.class, args);
        logger.trace("A TRACE Message");
        logger.debug("A DEBUG Message");
        logger.info("An INFO Message");
        logger.warn("A WARN Message");
        logger.error("An ERROR Message");
    }

}
