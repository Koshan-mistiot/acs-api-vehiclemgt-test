package com.mistiot.vehicleapi.repository;

/**
 * @author Koshan Samarasinghe
 * @date Oct 05, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepo extends JpaRepository<Vehicle,Long>{

    public Vehicle findByType(String type);
}
