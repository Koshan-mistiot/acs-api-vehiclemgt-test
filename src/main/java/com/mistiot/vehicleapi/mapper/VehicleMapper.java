package com.mistiot.vehicleapi.mapper;

/**
 * @author Koshan Samarasinghe
 * @date Oct 05, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.dto.VehicleDto;
import com.mistiot.vehicleapi.model.Vehicle;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VehicleMapper {

    VehicleMapper INSTANCE = Mappers.getMapper(VehicleMapper.class);

    VehicleDto fromVehicleEntityToDTo(Vehicle vehicle);

    Vehicle fromVehicleDtoToEntity(VehicleDto vehicleDto);

}
