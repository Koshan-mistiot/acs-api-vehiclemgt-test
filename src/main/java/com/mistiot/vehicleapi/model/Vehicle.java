package com.mistiot.vehicleapi.model;

/**
 * @author Koshan Samarasinghe
 * @date Oct 05, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Vehicle {

    @Id
    private long vehicleId;

    @Column(nullable = false,length = 200)
    private String vehicleIdentificationNo;

    @Column(nullable = false,length = 20)
    private String type;

    @Column(nullable = false,length = 500)
    private String description;

    public Vehicle() {
    }

    public Vehicle(long vehicleId, String vehicleIdentificationNo, String type, String description) {
        this.vehicleId = vehicleId;
        this.vehicleIdentificationNo = vehicleIdentificationNo;
        this.type = type;
        this.description = description;
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleIdentificationNo() {
        return vehicleIdentificationNo;
    }

    public void setVehicleIdentificationNo(String vehicleIdentificationNo) {
        this.vehicleIdentificationNo = vehicleIdentificationNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
