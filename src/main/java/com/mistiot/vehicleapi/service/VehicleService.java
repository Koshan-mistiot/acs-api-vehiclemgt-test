package com.mistiot.vehicleapi.service;

/**
 * @author Koshan Samarasinghe
 * @date Oct 05, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.common.ResourcePack;
import com.mistiot.vehicleapi.dto.VehicleDto;
import com.mistiot.vehicleapi.model.Vehicle;
import org.springframework.http.ResponseEntity;

public interface VehicleService {

    ResourcePack<VehicleDto> registerVehicle(VehicleDto vehicleDto);

    ResourcePack<VehicleDto> findByVehicleType(String type);

}
