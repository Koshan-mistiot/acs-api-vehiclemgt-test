package com.mistiot.vehicleapi.service.impl;

/**
 * @author Koshan Samarasinghe
 * @date Oct 06, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.common.ResourcePack;
import com.mistiot.vehicleapi.common.ResponseMessage;
import com.mistiot.vehicleapi.common.ResponseStatus;
import com.mistiot.vehicleapi.common.StatusCode;
import com.mistiot.vehicleapi.dto.VehicleDto;
import com.mistiot.vehicleapi.mapper.VehicleMapper;
import com.mistiot.vehicleapi.model.Vehicle;
import com.mistiot.vehicleapi.repository.VehicleRepo;
import com.mistiot.vehicleapi.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    private VehicleRepo vehicleRepo;

    @Override
    public ResourcePack<VehicleDto> registerVehicle(VehicleDto vehicleDto) {
        Vehicle savedObject = vehicleRepo.save(VehicleMapper.INSTANCE.fromVehicleDtoToEntity(vehicleDto));
        VehicleDto vehicleRegRes = VehicleMapper.INSTANCE.fromVehicleEntityToDTo(savedObject);
        ResourcePack<VehicleDto> resourcePack = new ResourcePack(vehicleRegRes, new ResponseStatus(StatusCode.SUCCESS, ResponseMessage.SUC001.getCode(), ResponseMessage.SUC001.getMessage()), HttpStatus.CREATED);
        return resourcePack;
    }

    @Override
    public ResourcePack<VehicleDto> findByVehicleType(String type) {
        Vehicle vehicle = vehicleRepo.findByType(type);
        VehicleDto vehicleRegRes = VehicleMapper.INSTANCE.fromVehicleEntityToDTo(vehicle);
        ResourcePack<VehicleDto> resourcePack = new ResourcePack(vehicleRegRes, new ResponseStatus(StatusCode.SUCCESS, ResponseMessage.SUC002.getCode(), ResponseMessage.SUC002.getMessage()), HttpStatus.FOUND);
        return resourcePack;
    }
}
