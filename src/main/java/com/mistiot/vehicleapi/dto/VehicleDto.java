package com.mistiot.vehicleapi.dto;

/**
 * @author Koshan Samarasinghe
 * @date Oct 05, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class VehicleDto {

    @JsonProperty("vehicleId")
    private long vehicleId;

    @JsonProperty("vehicleIdentificationNo")
    private String vehicleIdentificationNo;

    @JsonProperty("type")
    private String type;

    @JsonProperty("description")
    private String description;

    public VehicleDto() {
    }

    public VehicleDto(long vehicleId, String vehicleIdentificationNo, String type, String description) {
        this.vehicleId = vehicleId;
        this.vehicleIdentificationNo = vehicleIdentificationNo;
        this.type = type;
        this.description = description;
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleIdentificationNo() {
        return vehicleIdentificationNo;
    }

    public void setVehicleIdentificationNo(String vehicleIdentificationNo) {
        this.vehicleIdentificationNo = vehicleIdentificationNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
