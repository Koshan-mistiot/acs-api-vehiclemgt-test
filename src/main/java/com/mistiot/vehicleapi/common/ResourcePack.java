package com.mistiot.vehicleapi.common;

import org.springframework.http.HttpStatus;

/**
 * @author Koshan Samarasinghe
 * @version 0.1
 * @date Oct 06, 2020
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

public class ResourcePack<T> {
    private T data;
    private ResponseStatus responseStatus;
    private HttpStatus httpStatus;

    public ResourcePack(ResponseStatus responseStatus, HttpStatus httpStatus) {
        this.responseStatus = responseStatus;
        this.httpStatus = httpStatus;
    }

    public ResourcePack(T data, ResponseStatus responseStatus, HttpStatus httpStatus) {
        this.data = data;
        this.responseStatus = responseStatus;
        this.httpStatus = httpStatus;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

}


