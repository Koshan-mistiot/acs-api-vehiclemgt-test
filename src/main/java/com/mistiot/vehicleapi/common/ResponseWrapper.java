package com.mistiot.vehicleapi.common;

/**
 * @author Koshan Samarasinghe
 * @version 0.1
 * @date Oct 06, 2020
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseWrapper<T> {
    private T data;
    private String code;
    private String message;
    private String status;

    public ResponseWrapper() {
    }

    public ResponseWrapper(T data, String code, String message, String status) {
        this.data = data;
        this.code = code;
        this.message = message;
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
