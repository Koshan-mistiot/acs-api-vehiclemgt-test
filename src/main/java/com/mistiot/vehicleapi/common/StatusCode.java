package com.mistiot.vehicleapi.common;

/**
 * @author Koshan Samarasinghe
 * @version 0.1
 * @date Oct 06, 2020
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

public enum StatusCode {
    SUCCESS,
    ERROR,
    WARNING,

}
