package com.mistiot.vehicleapi.common;

/**
 * @author Koshan Samarasinghe
 * @version 0.1
 * @date Oct 06, 2020
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

public class ResponseStatus {

    private StatusCode statusCode;
    private String code;
    private String message;

    public ResponseStatus(StatusCode statusCode, String code, String message) {
        this.statusCode = statusCode;
        this.code = code;
        this.message = message;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
