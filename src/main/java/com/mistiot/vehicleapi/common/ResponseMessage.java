package com.mistiot.vehicleapi.common;

/**
 * @author Koshan Samarasinghe
 * @version 0.1
 * @date Oct 06, 2020
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

public enum ResponseMessage {

    SUC001("SUC001", "Car Registration successful"),
    SUC002("SUC002", "Value Retrieving successful"),
    ERR001("ERR001", "Car Registration error"),
    ERR002("ERR002", "Car already exists");

    private final String code;
    private final String message;

    ResponseMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
