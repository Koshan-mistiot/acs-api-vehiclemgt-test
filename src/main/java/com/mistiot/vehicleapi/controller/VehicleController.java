package com.mistiot.vehicleapi.controller;

/**
 * @author Koshan Samarasinghe
 * @date Oct 05, 2020
 * @version 0.1
 * @copyright © 2020 Mist Iot (Pvt) Ltd. All Rights Reserved
 */

import com.mistiot.vehicleapi.common.ResourcePack;
import com.mistiot.vehicleapi.common.ResponseWrapper;
import com.mistiot.vehicleapi.dto.VehicleDto;
import com.mistiot.vehicleapi.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/vehicle")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;


    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<ResponseWrapper<VehicleDto>> registerVehicle(@RequestBody VehicleDto vehicleDto) {

        ResourcePack<VehicleDto> vehicleRes = vehicleService.registerVehicle(vehicleDto);
        ResponseWrapper responseWrapper = new ResponseWrapper(vehicleRes.getData(), vehicleRes.getResponseStatus().getCode(), vehicleRes.getResponseStatus().getMessage(), vehicleRes.getResponseStatus().getStatusCode().name());
        ResponseEntity<ResponseWrapper<VehicleDto>> responseEntity = new ResponseEntity<>(responseWrapper, vehicleRes.getHttpStatus());
        return responseEntity;
    }

    @GetMapping(path = "/{type}", produces = "application/json")
    public ResponseEntity<ResponseWrapper<VehicleDto>> getVehicle(@PathVariable("type") String type) {

        ResourcePack<VehicleDto> vehicleRes = vehicleService.findByVehicleType(type);
        ResponseWrapper responseWrapper = new ResponseWrapper(vehicleRes.getData(), vehicleRes.getResponseStatus().getCode(), vehicleRes.getResponseStatus().getMessage(), vehicleRes.getResponseStatus().getStatusCode().name());
        ResponseEntity<ResponseWrapper<VehicleDto>> responseEntity = new ResponseEntity<>(responseWrapper, vehicleRes.getHttpStatus());
        return responseEntity;
    }


}
